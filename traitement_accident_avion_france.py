#!/usr/bin/ python3
# -*- coding: utf-8 -*-
"""
Accident d'avion et d'ULM ayant une cause technique
Source : https://bea.aero/les-enquetes/evenements-notifies/
Fabrice PEPIN - décembre 2022
"""


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Couleur par défaut des barplots
my_grey = (0.2, 0.2, 0.2)

df = pd.read_csv('donnees/accident_avion_technique_france_45.csv', sep=';')
print("Nombre d'évènements pour df :", df.shape[0])

#--- matplotlib parameters
sns.set_theme(style="white", palette="colorblind")

def bar_plot(dataframe, plot_size, col_list, plot_title, lab_size, img):
    """Trace le barplot, définie sa taille et exporte l'image"""
    global my_grey
    fig, ax = plt.subplots(figsize=plot_size)
    # Créer un container de type barh
    if col_list == 'my_grey':
        col_list = len(dataframe) * [my_grey]
    cont = ax.barh(dataframe.index,
                   dataframe.values,
                   color=col_list,
                   height=0.7  # Largeur des barres
                   )
    # Ajoute la valeur sur la barre en supprimant les décimales
    ax.bar_label(cont, fmt='%.0f', label_type='edge', padding=2, fontsize=lab_size)
    ax.set_title(plot_title, fontdict={'fontsize': 14}, loc='left')
    # Supprime les bords haut et droit
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    # Supprimer l'axe X
    ax.axes.get_xaxis().set_visible(False)
    fig.tight_layout(pad=0.9)
    img_name = img + '.png'
    plt.savefig(img_name, format='png', dpi=400)

def bar_plot2(dataframe1, dataframe2, plot_size, col_list, plot_title, lab_size, img):
    """Trace le barplot avec 2 catégories, définie sa taille et exporte l'image"""
    global my_grey
    width = 0.7 # Largeur des barres
    fig, ax = plt.subplots(figsize=plot_size)
    # Créer un container de type barh
    if col_list == 'my_grey':
        col_list = len(dataframe1) * [my_grey]
    cont1 = ax.barh(dataframe1.index - width/2,
                    dataframe1.values,
                    color=col_list,
                    height=width
                    )
    cont2 = ax.barh(dataframe2.index + width/2,
                    dataframe2.values,
                    color=col_list,
                    height=width
                    )
    # Ajoute la valeur sur la barre en supprimant les décimales
    ax.bar_label(cont1, fmt='%.0f', label_type='edge', padding=2, fontsize=lab_size)
    ax.bar_label(cont2, fmt='%.0f', label_type='edge', padding=2, fontsize=lab_size)

    ax.set_title(plot_title, fontdict={'fontsize': 14}, loc='left')
    # Supprime les bords haut et droit
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    # Supprimer l'axe X
    ax.axes.get_xaxis().set_visible(False)
    fig.tight_layout(pad=0.9)
    img_name = img + '.png'
    plt.savefig(img_name, format='png', dpi=400)


def somme_causes(dataframe):
    """Calcule la répartition de la somme des causes 1 à 4
        Le calcul comprend le décompte et la fréquence
        Les fréquences sont calculées par rapport à l'ensemble des données
        Variable : dataframe ou groupby
        Sortie : dictionnaire avec le décompte et le calul des fréquences
    """
    cause_1_count = dataframe['Cause_1'].value_counts(dropna=True)
    cause_2_count = dataframe['Cause_2'].value_counts(dropna=True)
    cause_3_count = dataframe['Cause_3'].value_counts(dropna=True)
    cause_4_count = dataframe['Cause_4'].value_counts(dropna=True)
    causes_count = cause_1_count.add(cause_2_count, fill_value=0)
    causes_count = causes_count.add(cause_3_count, fill_value=0)
    causes_count = causes_count.add(cause_4_count, fill_value=0)
    return causes_count

def prob(serie, dataframe):
    """Calcule le %age de la série à partir de la taille du dataframe"""
    # Il faut ordonner la série car matplotlib ne le fait pas
    probability = serie.sort_values() / dataframe.shape[0] * 100
    return probability


#--- Exploration des données
# Catégories
categorie = df['Cat.'].value_counts()
print('')
print('Catégorie :')
print(prob(categorie, df))
bar_plot(prob(categorie, df),
         (4.0, 1.5),
         my_grey,
         "Catégories d'avion (%)",
         10,
         '1_categories'
         )

# Evènements
evenement =  df['Evènement'].value_counts()
# Ajout des comptes d'évènement dans les causes
evenement = evenement.add(df['Cause_1'].value_counts(), fill_value=0)
evenement = evenement.drop(index=['Autre', 'Commandes', 'Hélice', 'Moteur',
                                   'Défaut concept.', 'Trains', 'Electricité'
                                 ]
                           )
print('')
print('Evènements :')
print(prob(evenement, df))
print('Cumul des fréquences :', prob(evenement, df).sum())
bar_plot(prob(evenement, df),
         (4.0, 3.0),
         my_grey,
         'Evènements (%)',
         10,
         '1_evenement'
         )


# Conséquences
consequence = df['Conséquences'].value_counts()
print('')
print('Conséquence :')
print(prob(consequence, df))
bar_plot(prob(consequence, df),
         (4.0, 2.1),
         [my_grey, my_grey, 'blue', 'blue', 'blue'],
         'Conséquences (%)',
         10,
         '1_consequence'
         )

# Certitude
certitude = df['Certitude'].value_counts()
print('')
print('Certitude sur la cause:')
print(prob(certitude, df))


# Cumul des causes
causes_toutes = somme_causes(df)
# Causes techniques
causes_tech = causes_toutes.drop(index=['Att. forcé', 'Intégrité vol',
                                           'Prévol', 'Intégrité sol',
                                           'Interruption vol', 'Défaut concept.',
                                           'Défaut constr.', 'Défaut entret.'
                                          ]
                                    )
# Causes techniques/systèmes
causes_systeme = causes_tech.drop(index=['Allumage', 'Carb. indisp.', 'Huile',
                                         'Givrage', 'Carb. pompe',
                                         'Carb. contam.'
                                         ]

                                  )
print('')
print("Fréquence des causes liées aux systèmes rapportée au nombre total d'évènements :")
print(prob(causes_systeme, df))
print('Cumul des fréquences :', prob(causes_systeme, df).sum())
bar_plot(prob(causes_systeme, df),
         (4.0, 3.5),
         [my_grey, my_grey, my_grey, my_grey, my_grey, my_grey, my_grey,
          my_grey, 'blue'],
         'Causes - systèmes (%)',
         10,
         '2_causes_systemes'
         )

# Causes techniques/moteur/systèmes_moteur
causes_mot1 = df[df['Cause_1']=='Moteur']['Cause_2'].value_counts()
causes_mot2 = df[df['Cause_1']=='Moteur']['Cause_3'].value_counts()
causes_mot3 = df[df['Cause_1']=='Moteur']['Cause_4'].value_counts()
causes_mot4 = df[df['Cause_2']=='Moteur']['Cause_1'].value_counts()
causes_mot5 = df[df['Cause_2']=='Moteur']['Cause_3'].value_counts()
causes_mot6 = df[df['Cause_2']=='Moteur']['Cause_4'].value_counts()
causes_mot7 = df[df['Cause_3']=='Moteur']['Cause_1'].value_counts()
causes_mot8 = df[df['Cause_3']=='Moteur']['Cause_2'].value_counts()
causes_mot9 = df[df['Cause_3']=='Moteur']['Cause_4'].value_counts()
causes_mot = causes_mot1.add(causes_mot2, fill_value=0)
causes_mot = causes_mot.add(causes_mot3, fill_value=0)
causes_mot = causes_mot.add(causes_mot4, fill_value=0)
causes_mot = causes_mot.add(causes_mot5, fill_value=0)
causes_mot = causes_mot.add(causes_mot6, fill_value=0)
causes_mot = causes_mot.add(causes_mot7, fill_value=0)
causes_mot = causes_mot.add(causes_mot8, fill_value=0)
causes_mot = causes_mot.add(causes_mot9, fill_value=0)
# Calcul des fréquences des causes de problème moteur
causes_mot_prob = causes_mot / causes_toutes['Moteur'] *100
causes_mot_prob = causes_mot_prob.sort_values(ascending=False)
# Suppression des conséquences des problèmes moteur
causes_mot_prob = causes_mot_prob.drop(index=['Att. forcé',
                                              'Interruption vol',
                                              'Intégrité sol',
                                              'Trains'
                                             ]
                                       )
print('')
print('Causes des problèmes moteur rapportées au nombre de problèmes moteur :')
print(causes_mot_prob)
print('Cumul des fréquences :', causes_mot_prob.sum())
bar_plot(causes_mot_prob.sort_values(ascending=True),
         (4.5, 5.9),
         my_grey,
         'Causes - moteur (%)',
         10,
         '2_causes_mot'
         )

causes_mot_systeme_prob = causes_mot_prob.drop(index=['Défaut concept.',
                                                      'Défaut constr.',
                                                      'Défaut entret.',
                                                      'Prévol',
                                                      'Réducteur'
                                                      ]
                                               )
print('')
print('Causes des systèmes moteur rapportées au nombre de problèmes moteur :')
print(causes_mot_systeme_prob)
print('Cumul des fréquences :', causes_mot_systeme_prob.sum())
bar_plot(causes_mot_systeme_prob.sort_values(ascending=True),
         (4.7, 3.5),
         'blue',
         'Causes - systèmes moteur (% / Moteur)',
         10,
         '2_causes_systemes_mot'
         )

# Les causes humaines
causes_humaines = causes_toutes.drop(index=['Allumage', 'Att. forcé', 'Autre',
                                            'Aviaire', 'Carb. contam.',
                                            'Carb. indisp.', 'Carb. pompe',
                                            'Commandes', 'Electricité',
                                            'Givrage','Hélice', 'Huile',
                                            'Moteur', 'Réducteur',
                                            'Att. forcé', 'Interruption vol',
                                            'Intégrité sol', 'Intégrité vol',
                                            'Feu',
                                            'Trains'
                                            ]
                                     )
print('')
print('Causes humaines des problèmes techniques rapportées au nombre total:')
print(prob(causes_humaines, df))
print('Cumul des fréquences :', prob(causes_humaines, df).sum())
bar_plot(prob(causes_humaines, df).sort_values(ascending=True),
         (4.5, 1.8),
         my_grey,
         'Causes - humaines (%)',
         10,
         '1_causes_humaines'
         )

#--- Analyse des causes des évènements
# Collision au sol
print('')
print('Pour les évènements "Collision au sol":')
collision_sol = df.loc[df['Evènement']=='Collision au sol']['Cause_1'].value_counts()
collision_sol_prob = collision_sol / collision_sol.sum() * 100
print(collision_sol_prob)
bar_plot(collision_sol_prob.sort_values(ascending=True),
         (4.5, 3.1),
         my_grey,
         'Causes - collisions au sol (%)',
         10,
         '2_causes_collision_sol'
         )

# Atterrissage forcé
print('')
print('Pour les évènements "Att. forcé":')
att_force = df.loc[df['Evènement']=='Att. forcé']['Cause_1'].value_counts()
att_force_prob = att_force / att_force.sum() * 100
print(att_force_prob)
bar_plot(att_force_prob.sort_values(ascending=True),
         (4.5, 3.1),
         my_grey,
         'Causes - atterrissage forcé (%)',
         10,
         '2_causes_att_force'
         )

# Interruption du vol
print('')
print('Pour les évènements "Interruption vol":')
interruption_vol = df.loc[df['Evènement']=='Interruption vol']['Cause_1'].value_counts()
interruption_vol_prob = interruption_vol / interruption_vol.sum() * 100
print(interruption_vol_prob)
bar_plot(interruption_vol_prob.sort_values(ascending=True),
         (4.5, 3.3),
         my_grey,
         'Causes - interruptions de vol (%)',
         10,
         '2_causes_interruption_vol'
         )

# Intégrité au sol
print('')
print('Pour les évènements "Intégrité sol":')
integrite_sol = df.loc[df['Evènement']=='Intégrité sol']['Cause_1'].value_counts()
integrite_sol_prob = integrite_sol / integrite_sol.sum() * 100
print(integrite_sol_prob)
bar_plot(integrite_sol_prob.sort_values(ascending=True),
         (4.5, 1.2),
         my_grey,
         'Causes - intégrité au sol (%)',
         10,
         '2_causes_integrite_sol'
         )

# Crash sans contrôle
print('')
print('Pour les évènements "Crash sans ctrl":')
crash_sans_ctrl = df.loc[df['Evènement']=='Crash sans ctrl']['Cause_1'].value_counts()
crash_sans_ctrl_prob = crash_sans_ctrl / crash_sans_ctrl.sum() * 100
print(crash_sans_ctrl_prob)
bar_plot(crash_sans_ctrl_prob.sort_values(ascending=True),
         (4.5, 2.0),
         my_grey,
         'Causes - crash sans contrôle (%)',
         10,
         '2_causes_crash_sans_ctrl'
         )

# Intégrité en vol
print('')
print('Pour les évènements "Intégrité vol":')
integrite_vol = df.loc[df['Evènement']=='Intégrité vol']['Cause_1'].value_counts()
integrite_vol_prob = integrite_vol / integrite_vol.sum() * 100
print(integrite_vol_prob)
bar_plot(integrite_vol_prob.sort_values(ascending=True),
         (4.5, 1.2),
         my_grey,
         'Causes - intégrité en vol (%)',
         10,
         '2_causes_integrite_vol'
         )

# Feu
print('')
print('Pour les évènements "Feu":')
feu1 = df.loc[df['Cause_1']=='Feu']['Cause_2'].value_counts()
feu2 = df.loc[df['Cause_2']=='Feu']['Cause_2'].value_counts()
feu = feu1.add(feu2, fill_value=0)
feu_prob = feu / feu.sum() * 100
print(feu_prob)
bar_plot(feu_prob.sort_values(ascending=True),
         (4.5, 2.0),
         my_grey,
         'Causes - feu (%)',
         10,
         '2_causes_feu'
         )

#


"""
# Tracé des répartitions pour les causes 1 à 4
# A lancer la fonction en phase exploratoire uniquement
def causes_1_a_4():
    # Il faut supprimer les NaN car ce sont des floats
    cause_1_prob =  df['Cause_1'].value_counts(dropna=True, normalize=True, ascending=True)*100
    bar_plot(cause_1_prob,
             (5.2, 6.2),
             [(0.2, 0.2, 0.2)],
             'Cause 1 (%)',
             10,
             'cause_1'
             )
    cause_2_prob =  df['Cause_2'].value_counts(dropna=True, normalize=True, ascending=True)*100
    bar_plot(cause_2_prob,
             (5.2, 5.6),
             [(0.2, 0.2, 0.2)],
             'Cause 2 (%)',
             10,
             'cause_2'
             )
    cause_3_prob =  df['Cause_3'].value_counts(dropna=True, normalize=True, ascending=True)*100
    bar_plot(cause_3_prob,
             (5.2, 5.6),
             [(0.2, 0.2, 0.2)],
             'Cause 3 (%)',
             10,
             'cause_3'
             )
    cause_4_prob =  df['Cause_4'].value_counts(dropna=True, normalize=True, ascending=True)*100
    bar_plot(cause_4_prob,
             (5.2, 1.2),
             [(0.2, 0.2, 0.2)],
             'Cause 4 (%)',
             10,
             'cause_4'
             )
"""
